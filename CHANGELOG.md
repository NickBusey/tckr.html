v0.3
------------------
* Added command to list variables
* Added ability to load and save multiple ticker tapes

v0.2
------------------
* Added historical editing
* Added import/export
* Improved note handling

v0.1
------------------
* Initial Release