# tckr.html

tckr.html (said `ticker`) is a simple calculator strip. Like old ticker tape, with variables.
It's meant to be used in a scratch pad sort of fashion, for quick calculations and note taking.
It can be great for brewing, woodworking, accounting, anything where you need to work with
several related numbers quickly.

## Demo

* [Hosted Version](https://nickbusey.gitlab.io/tckr.html/)
* [JSFiddle Demo](https://jsfiddle.net/7kn0rqbf/)

## Features

* Self contained in a single html file
* Assign numbers to variables for later use
* Write comments freely
* Saves output between page loads with local storage
* Load/Save multiple ticker tapes
* Import/Export
* One click copy of output
* Edit previous values and the whole page updates

## Screenshots

![Screenshot](screenshot.png)

## Usage

Download the latest version, open it in your browser, type some math.

Type `/help` or `/h` for all commands.

## Examples

### Assigning a variable

```
price=10
    Assigned price to: 10
price*20
    200
```

### Assigning the last result to a variable

```
5+5
    10
=price
    Assigned price to: 10
price*20
    200
```
